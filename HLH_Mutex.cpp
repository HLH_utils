/*******************************************************************************
 *    File Name : HLH_Mutex.cpp
 * 
 *       Author : Henry He
 * Created Time : 2009-10-8 11:10:40
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include "HLH_utils/HLH_Mutex.h"
#include "HLH_utils/typedef.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/





/******************************************************************************
 * Desc : Member Functions
 ******************************************************************************/





/******************************************************************************
 * Func : HLH_Mutex::HLH_Mutex
 * Desc : Constructor of HLH_Mutex
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
HLH_Mutex::HLH_Mutex ()
{
  // Notify that this mutex is not initialized
  m_bInited = false;
}


/******************************************************************************
 * Func : HLH_Mutex::~HLH_Mutex
 * Desc : Deconstructor of HLH_Mutex
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
HLH_Mutex::~ HLH_Mutex ()
{
  Destroy ();
}


/******************************************************************************
 * Func : HLH_Mutex::Destroy
 * Desc : Destroy this mutex
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void HLH_Mutex::Destroy ()
{
  if (m_bInited) {
    // Destroy this mutex
    pthread_mutex_destroy(&m_mMutex);
    // Notify that this mutex is not initialized
    m_bInited = false;
		m_bLocked = false;
  }
}

/******************************************************************************
 * Func : HLH_Mutex::Init
 * Desc : Init this mutex
 * Args : NONE
 * Outs : if success return 0, otherwise return -1
 ******************************************************************************/
int HLH_Mutex::Init ()
{
  if (m_bInited) {
		HLH_DEBUG ( HLH_DEBUG_UTILS, ("already inited") );
    return HLH_MUTEX_ERR_ALREADY_INITED;
	}

  // Initialize this mutex
  pthread_mutex_init (&m_mMutex, NULL);

  // Notify that this mutex is initialized
  m_bInited = true;
	m_bLocked = false;

  return 0; 
}


/******************************************************************************
 * Func : HLH_Mutex::Lock
 * Desc : Lock this mutex
 * Args : NONE
 * Outs : if success return 0, otherwise return -1
 ******************************************************************************/
int HLH_Mutex::Lock ()
{
  if (!m_bInited) {
		HLH_DEBUG ( HLH_DEBUG_UTILS, ("not inited") );
    return HLH_MUTEX_ERR_NOT_INIT;
	}

  pthread_mutex_lock ( &m_mMutex );
	m_bLocked = true;

  return 0;
}


/******************************************************************************
 * Func : HLH_Mutex::Unlock
 * Desc : Unlock this mutex
 * Args : NONE
 * Outs : if success return 0, otherwise return -1
 ******************************************************************************/
int HLH_Mutex::Unlock ()
{
  if (!m_bInited) {
		HLH_DEBUG ( HLH_DEBUG_UTILS, ("not inited") );
    return HLH_MUTEX_ERR_NOT_INIT;
	}

  pthread_mutex_unlock ( &m_mMutex );
	m_bLocked = false;

  return 0;
}

