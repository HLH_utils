/*******************************************************************************
 *         File : HLH_Mutex.h
 * 
 *       Author : Henry He
 *      Created : 2009-10-8 11:08:38
 *  Description : 
 ******************************************************************************/

#ifndef __HLH_MUTEX_INC_20091008_110838_HENRY__
#define __HLH_MUTEX_INC_20091008_110838_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include	<pthread.h>


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/
#define HLH_MUTEX_ERR_FAILED                  (-1)
#define HLH_MUTEX_ERR_ALREADY_INITED          (-2)
#define HLH_MUTEX_ERR_NOT_INIT                (-3)
#define HLH_MUTEX_ERR_CANT_INIT               (-4)



/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Classes
 ******************************************************************************/
class HLH_Mutex
{

  friend class HLH_Cond;

public:
  /******************************************************************************
   * Desc : Constructor / Deconstructor of HLH_Mutex
   ******************************************************************************/

  // Constructor of HLH_Mutex
  HLH_Mutex();

  // Deconstructor of HLH_Mutex
  ~HLH_Mutex();

public:
  /******************************************************************************
   * Desc : Operations
   ******************************************************************************/

  // Init this mutex
  int Init ();

  // Destroy this mutex
  void Destroy ();

  // Lock this mutex
  int Lock ();

  // Unlock this mutex
  int Unlock ();

  // Whether this mutex initialized
  bool IsInited () { return m_bInited; }

	bool IsLocked () { return m_bLocked; }

private:
  /******************************************************************************
   * Desc : Private Data
   ******************************************************************************/
  // Whether this mutex created
  bool                m_bInited;

	// Whether this mutex locked;
	bool 								m_bLocked;

  // Internal mutex of HLH_Mutex
  pthread_mutex_t     m_mMutex;

};



/******************************************************************************
 * Desc : Auto initialized HLH_Mutex
 ******************************************************************************/
class HLH_AutoMutex : public HLH_Mutex
{
  public:
    HLH_AutoMutex () { HLH_Mutex::Init (); }

};






#endif /* __HLH_MUTEX_INC_20091008_110838_HENRY__ */
