/*******************************************************************************
 *         File : HLH_Ref.h
 * 
 *       Author : Henry He
 *      Created : Wed 18 Nov 2009 03:59:55 PM CST
 *  Description : 
 ******************************************************************************/

#ifndef __HLH_REF_INC_20091118_155955_HENRY__
#define __HLH_REF_INC_20091118_155955_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/
#define HLH_REF_ERR_FAILED          (-1)
#define HLH_REF_ERR_ALREADY_REFERED (-2)
#define HLH_REF_ERR_NOT_REFERED     (-3)
#define HLH_REF_ERR_CANNT_REFER     (-4)


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Functions
 ******************************************************************************/



/******************************************************************************
 * Class : HLH_Ref
 * Desc  : 
 ******************************************************************************/
template <class T>
class HLH_Ref
{

  public:

    //=====================  TYPEDEFS  =============================================

    // Type of release function
    typedef void (*ReleaseThisFunc)  (T *pvThis);
    typedef void (*ReleaseOtherFunc) (T *pvThis, void *pvOther);
    typedef enum RefType {REF_TYPE_NEW, REF_TYPE_THIS, REF_TYPE_OTHER}

    // ====================  LIFECYCLE    ========================================= 

    // Constructor of HLH_Ref: refer to HLH_Ref
    HLH_Ref (const HLH_Ref &zhrRef, UINT32 unRef = 1) {
      Ref (zhrRef, unRef);
    }

    // Constructor of HLH_Ref: refer to a pointer created by new method
    explicit HLH_Ref (T *ptObj = NULL, UINT32 unRef = 1) {
      RefNew (ptObj, unRef);
    }

    // Constructor of HLH_Ref: refer to a pointer created by free method
    explicit HLH_Ref (T *ptObj = NULL, ReleaseThisFunc zrtRelease = free,
        bool bDeconstruct = true, UINT32 unRef = 1) {
      RefThis (ptObj, zrtRelease, bDeconstruct, unRef);
    }

    // Constructor of HLH_Ref: refer to a pointer that takes this point and
    // other pointer to release it
    explicit HLH_Ref (T *ptObj = NULL, ReleaseOtherFunc zroRelease = NULL,
        void *pvOther = NULL, bool bDeconstruct = false, pvUINT32 unRef = 1) {
      RefOther (ptObj, rfRelease, bDeconstruct, unRef);
    }

    // Decontructor of HLH_Ref
    ~ HLH_Ref () { Destroy (); }


  public:

    // ====================  OPERATORS    ========================================= 

    // Reload of assignment from HLH_Ref
    HLH_Ref<T> & operator = (const HLH_Ref<T> & zhrRef);

    // Reload of assignment from pointer of class T
    HLH_Ref<T> & operator = (T *ptObj);

    // Reload of address resolve operator
    T & operator * () const;  

    // Reload of member access operator
    T * operator -> () const;  



    // ====================  OPERATIONS   ========================================= 

    // Create a new T class and refer to this new class
    int New (UINT32 unRef);

    // Refer to HLH_Ref
    int Ref (const HLH_Ref &zhrRef, UINT32 unRef = 1);

    // Refer to a pointer created by new method
    explicit int RefNew (T *ptObj = NULL, UINT32 unRef = 1);

    // Refer to a pointer created by free method
    explicit int RefThis (T *ptObj = NULL, ReleaseThisFunc zrtRelease = free,
        bool bDeconstruct = true, UINT32 unRef = 1);

    // Refer to a pointer that takes this point and other pointer to release it
    explicit int RefOther (T *ptObj = NULL, ReleaseOtherFunc zroRelease = NULL,
        void *pvOther = NULL, bool bDeconstruct = false, pvUINT32 unRef = 1);

    // Add reference to this pointer (avoid to use this after all)
    int AddRef (UINT32 unRef);

    // Release reference to this pointer (avoid to use this after all)
    int Release (UINT32 unRef);

    // Destroy the object refered
    void Destroy ();



    // ====================  ACCESS       ========================================= 

    // Get internal pointer
    T * Get() const;  

    // Reset the pointer to the initial pointer
    void Reset(T * p);  



  private:

    // ====================  OPERATIONS   ========================================= 


  private:

    // ====================  MEMBER DATA  ========================================= 



    // Reference count of this reference
    UINT32            m_unRef;

    // Pointer to global reference count
    UINT32           *m_punRef;



    // Pointer of this reference
    T                *m_ptObj;

    // Whether deconstructor should be called
    bool              m_bDeconstruct;

    // Type of reference
    RefType           m_zrtRefType;

    // Release functions of REF_TYPE_OTHER type
    void             *m_pvReleaseFunc;

    // Pointer to the other argument need by release function of REF_TYPE_OTHER type
    void             *m_pvReleaseOther;


};  // -----  end of class  HLH_Ref  ----- 





// ====================  OPERATORS    ========================================= 



/******************************************************************************
 * Func : operator = 
 * Desc : Reload of assignment from HLH_Ref
 * Args : zhrRef            HLH_Ref object assigned from
 * Outs : return reference of this object
 ******************************************************************************/
HLH_Ref<T> & HLH_Ref::operator = (const HLH_Ref<T> & zhrRef)
{
  Ref (zhrRef);
  return *this;
}

/******************************************************************************
 * Func : operator =
 * Desc : Reload of assignment from pointer of class T created by new
 * Args : ptObj               pointer to refer
 * Outs : return reference of this HLH_Ref
 ******************************************************************************/
HLH_Ref<T> & HLH_Ref::operator = (T *ptObj)
{
  RefNew (ptObj);
  return *this;
}

/******************************************************************************
 * Func : operator *
 * Desc : Reload of address resolve operator
 * Args : NONE
 * Outs : return reference of this pointer
 ******************************************************************************/
T & HLH_Ref::operator * () const
{
  return *m_ptObj;
}



/******************************************************************************
 * Func : operator ->
 * Desc : Reload of member access operator
 * Args : NONE
 * Outs : return this pointer
 ******************************************************************************/
T * HLH_Ref::operator -> () const
{
  return m_ptObj;
}




// ====================  OPERATIONS   ========================================= 




/******************************************************************************
 * Func : HLH_Ref::New
 * Desc : Create a new T class and refer to this new class
 * Args : unRef               initial reference count
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int HLH_Ref::New (UINT32 unRef)
{
  int nRetVal;
  T * ptObj = NULL;

  Destroy ();

  ptObj = new T;
  if (ptObj == NULL) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("create new T class failed") );
    return HLH_REF_ERR_FAILED;
  }

  nRetVal = RefNew (ptObj, unRef);

  return nRetVal;
}

/******************************************************************************
 * Func : HLH_Ref::Ref
 * Desc : Refer to HLH_Ref
 * Args : zhrRef        HLH_Ref instance to be refered
 *        unRef         initial reference count of this HLH_Ref
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
  template <class T>
int HLH_Ref::Ref (const HLH_Ref &zhrRef, UINT32 unRef = 1)
{
  // Destroy this reference first
  Destroy ();

  // Copy member
  m_punRef         = zhrRef.m_punRef;
  m_ptObj          = zhrRef.m_ptObj;
  m_bDeconstruct   = zhrRef.m_bDeconstruct;
  m_zrtRefType     = zhrRef.m_zrtRefType;
  m_pvReleaseFunc  = zhrRef.m_pvReleaseFunc;
  m_pvReleaseOther = zhrRef.m_pvReleaseOther;

  // Add reference only if not empty reference
  m_unRef          = unRef;

  if (m_ptObj) {
    ASSERT (zhrRef.m_unRef != 0);
    ASSERT (zhrRef.m_punRef != NULL);
    ASSERT (zhrRef.m_unRef <= *zhrRef.m_punRef);

    *m_punRef += m_unRef;
  }

  return *this;
}


/******************************************************************************
 * Func : HLH_Ref::RefNew
 * Desc : Refer to a pointer created by new method
 * Args : ptObj               pointer of object to be refered
 *        unRef               initial reference count of this pointer
 * Outs : 
 ******************************************************************************/
  template <class T>
int HLH_Ref::RefNew (T *ptObj = NULL, UINT32 unRef = 1)
{
  UINT32 *punRef = NULL;

  // Destroy this reference first
  Destroy ();

  // Check if input pointer empty
  if (ptObj == NULL) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("refer NULL pointer") );
    return HLH_REF_ERR_CANNT_REFER;
  }

  // Refer this pointer
  punRef = (UINT32*) malloc ( sizeof(UINT32) );
  if (punRef == NULL) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("memory allocate global reference count failed") );
    goto failed;
  }

  m_ptObj          = ptObj;
  m_unRef          = unRef;
  *punRef          = unRef;
  m_punRef         = punRef;
  m_bDeconstruct   = false;
  m_zrtRefType     = REF_TYPE_NEW;
  m_pvReleaseFunc  = NULL;
  m_pvReleaseOther = NULL;

  return 0;

failed:
  if (punRef != NULL) {
    free (punRef);
    punRef = NULL;
  }

  return HLH_REF_ERR_CANNT_REFER;
}

/******************************************************************************
 * Func : HLH_Ref::RefThis
 * Desc : Refer to a pointer that takes only this pointer to release
 * Args : ptObj               pointer of object to be refered
 *        zrtRelease          release function of \c ptObj
 *        bDeconstruct        whether this T class should be deconstruct before
 *                                release
 *        unRef               initial reference count of this pointer
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
  template <class T>
int RefThis (T *ptObj = NULL, ReleaseThisFunc zrtRelease = free,
    bool bDeconstruct = true, UINT32 unRef = 1)
{
  UINT32 *punRef = NULL;

  // Destroy this reference first
  Destroy ();

  // Check if input pointer empty
  if (ptObj == NULL) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("refer NULL pointer") );
    return HLH_REF_ERR_CANNT_REFER;
  }

  // Refer this pointer
  punRef = (UINT32*) malloc ( sizeof(UINT32) );
  if (punRef == NULL) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("memory allocate global reference count failed") );
    goto failed;
  }

  m_ptObj          = ptObj;
  m_unRef          = unRef;
  *punRef          = unRef;
  m_punRef         = punRef;
  m_bDeconstruct   = bDeconstruct;
  m_zrtRefType     = REF_TYPE_THIS;
  m_pvReleaseFunc  = zrtRelease;
  m_pvReleaseOther = NULL;

  return 0;

failed:
  if (punRef != NULL) {
    free (punRef);
    punRef = NULL;
  }

  return HLH_REF_ERR_CANNT_REFER;
}

/******************************************************************************
 * Func : HLH_Ref::RefOther
 * Desc : Refer to a pointer that takes this point and other pointer to release it
 * Args : ptObj               pointer of object to be refered
 *        zroRelease          release function of \c ptObj
 *        pvOther             the other argument of zroRelease
 *        bDeconstruct        whether this T class should be deconstruct before
 *                                release
 *        unRef               initial reference count of this pointer
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
  template <class T>
int HLH_Ref::RefOther (T *ptObj = NULL, ReleaseOtherFunc zroRelease = NULL,
    void *pvOther = NULL, bool bDeconstruct = false, pvUINT32 unRef = 1)
{
  UINT32 *punRef = NULL;

  // Destroy this reference first
  Destroy ();

  // Check if input pointer empty
  if (ptObj == NULL) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("refer NULL pointer") );
    return HLH_REF_ERR_CANNT_REFER;
  }

  // Refer this pointer
  punRef = (UINT32*) malloc ( sizeof(UINT32) );
  if (punRef == NULL) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("memory allocate global reference count failed") );
    goto failed;
  }

  m_ptObj          = ptObj;
  m_unRef          = unRef;
  *punRef          = unRef;
  m_punRef         = punRef;
  m_bDeconstruct   = bDeconstruct;
  m_zrtRefType     = REF_TYPE_OTHER;
  m_pvReleaseFunc  = zroRelease;
  m_pvReleaseOther = pvOther;

  return 0;

failed:
  if (punRef != NULL) {
    free (punRef);
    punRef = NULL;
  }

  return HLH_REF_ERR_CANNT_REFER;
}

/******************************************************************************
 * Func : HLH_Ref::AddRef
 * Desc : Add reference to this pointer (avoid to use this after all)
 * Args : unRef                   reference count to add
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
  template <class T>
int HLH_Ref::AddRef (UINT32 unRef)
{
  if (m_ptObj == NULL) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("add reference to empty reference") );
    return HLH_REF_ERR_NOT_REFERED;
  }

  ASSERT (m_unRef != 0);
  ASSERT (m_punRef != NULL);
  ASSERT (*m_punRef >= m_unRef);

  // Add reference to this pointer
  m_unRef += unRef;
  *m_punRef += unRef;

  return 0;
}

/******************************************************************************
 * Func : HLH_Ref::Release
 * Desc : Release reference to this pointer (avoid to use this after all)
 * Args : unRef             reference count to release
 * Outs : If success return the number of reference count released,
 *        otherwise return error code
 ******************************************************************************/
  template <class T>
int HLH_Ref::Release (UINT32 unRef)
{
  if (m_ptObj == NULL) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("release empty reference") );
    return HLH_REF_ERR_NOT_REFERED;
  }

  ASSERT (m_unRef != 0);
  ASSERT (m_punRef != NULL);
  ASSERT (*m_punRef >= m_unRef);

  // release reference to this pointer
  if (unRef > m_unRef) {
    unRef = m_unRef;
    HLH_DEBUG ( HLH_DEBUG_UTILS,
        ("reference count released larger than this reference count") );
  }
  m_unRef   -= unRef;
  *m_punRef -= unRef;

  // Release this pointer if this reference count run out
  if (m_unRef == 0) {
    Destroy ();
  }

  return unRef;
}

/******************************************************************************
 * Func : HLH_Ref::Destroy
 * Desc : Destroy the object refered,
 *        if release function is free(), deconstructor will be called
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
  template <class T>
void HLH_Ref::Destroy ()
{
  if (m_ptObj == NULL) {
    return;
  }

  ASSERT (m_unRef != 0);
  ASSERT (m_punRef != NULL);
  ASSERT (*m_punRef >= m_unRef);

  *m_punRef -= m_unRef;
  m_unRef = 0;

  // Release this object if global reference run out
  if (*m_punRef == 0) {

    // Deconstruct this T class if requested
    if ( m_bDeconstruct ) {
      m_ptObj->T::~T ();
    }

    switch (m_zrtRefType) {
      case REF_TYPE_NEW:
        delete m_ptObj;
        break;
      case REF_TYPE_THIS:
        ((ReleaseThisFunc)m_pvReleaseFunc) (m_ptObj);
        break;
      case REF_TYPE_OTHER:
        ((ReleaseOtherFunc)m_pvReleaseFunc) (m_ptObj);
        break;
      default:
        HLH_DEBUG ( HLH_DEBUG_UTILS,
            ("unknown reference type %u", (UINT32)m_zrtRefType) );
        break;
    }

    // Free global reference count space
    free (m_punRef);

  }

  m_ptObj  = NULL;
  m_punRef = NULL;

}



#endif /* __HLH_REF_INC_20091118_155955_HENRY__ */
