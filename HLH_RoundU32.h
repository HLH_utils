/*******************************************************************************
 *         File : ..\utils\HLH_RoundU32.h
 * 
 *       Author : Henry He
 *      Created : 2009-10-14 14:26:15
 *  Description : 
 ******************************************************************************/

#ifndef __HLH_ROUNDU32_INC_20091014_142615_HENRY__
#define __HLH_ROUNDU32_INC_20091014_142615_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include "HLH_utils/typedef.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/
#define HLH_ROUNDU32_RANGE            (  ~ ( (UINT32) 0 )  )
#define HLH_ROUNDU32_HALF             ( HLH_ROUNDU32_RANGE / 2 )


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Classes
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Class to handle round up counter

class HLH_RoundU32
{
  public:
    /******************************************************************************
     * Desc : Constructor / Deconstructor
     ******************************************************************************/
    HLH_RoundU32 (UINT32 unVal = 0) { m_unVal = unVal; }

    ~HLH_RoundU32 () {}

  public:
    /******************************************************************************
     * Desc : Convertions
     ******************************************************************************/
		UINT32	GetUINT32 ();
    operator UINT32 () const      { return m_unVal; }
    operator float  () const      { return (float) m_unVal; }
    operator double () const      { return (double) m_unVal; }

  public:
    /******************************************************************************
     * Desc : Operators
     ******************************************************************************/
    // Preposed add
    HLH_RoundU32 &operator ++ ()  { ++m_unVal; return *this; }

    // Preposed sub
    HLH_RoundU32 &operator -- ()  { --m_unVal; return *this; }

    // Postpositioned add
    HLH_RoundU32  operator ++ (int)  { return HLH_RoundU32 (m_unVal++); }

    // Postpositioned sub
    HLH_RoundU32  operator -- (int)  { return HLH_RoundU32 (m_unVal--); }

    HLH_RoundU32  operator +  (const HLH_RoundU32 &zhrVal) const { return HLH_RoundU32 (m_unVal + zhrVal.m_unVal); }
    HLH_RoundU32  operator -  (const HLH_RoundU32 &zhrVal) const { return HLH_RoundU32 (m_unVal - zhrVal.m_unVal); }
    HLH_RoundU32  operator *  (const HLH_RoundU32 &zhrVal) const { return HLH_RoundU32 (m_unVal * zhrVal.m_unVal); }
    HLH_RoundU32  operator /  (const HLH_RoundU32 &zhrVal) const { return HLH_RoundU32 (m_unVal / zhrVal.m_unVal); }
    HLH_RoundU32  operator %  (const HLH_RoundU32 &zhrVal) const { return HLH_RoundU32 (m_unVal % zhrVal.m_unVal); }

    HLH_RoundU32 &operator =  (UINT32 unVal) { m_unVal = unVal; return *this; }

    HLH_RoundU32 &operator += (const HLH_RoundU32 &zhrVal) { m_unVal += zhrVal.m_unVal; return *this; }
    HLH_RoundU32 &operator -= (const HLH_RoundU32 &zhrVal) { m_unVal -= zhrVal.m_unVal; return *this; }
    HLH_RoundU32 &operator *= (const HLH_RoundU32 &zhrVal) { m_unVal *= zhrVal.m_unVal; return *this; }
    HLH_RoundU32 &operator /= (const HLH_RoundU32 &zhrVal) { m_unVal /= zhrVal.m_unVal; return *this; }
    HLH_RoundU32 &operator %= (const HLH_RoundU32 &zhrVal) { m_unVal %= zhrVal.m_unVal; return *this; }

    bool operator <  (const HLH_RoundU32 &zhrVal) const;
    bool operator >  (const HLH_RoundU32 &zhrVal) const;
    bool operator <= (const HLH_RoundU32 &zhrVal) const;
    bool operator >= (const HLH_RoundU32 &zhrVal) const;

    bool operator == (const HLH_RoundU32 &zhrVal) const { return ( m_unVal == zhrVal.m_unVal ); }
    bool operator != (const HLH_RoundU32 &zhrVal) const { return ( m_unVal != zhrVal.m_unVal ); }

  public:
    UINT32      m_unVal;
};




/******************************************************************************
 * Desc : Operators
 ******************************************************************************/


inline bool HLH_RoundU32::operator <  (const HLH_RoundU32 &zhrVal) const
{

  UINT32  unDivide;
  UINT32  unVal;

  unVal    = zhrVal.m_unVal;
  unDivide = unVal + HLH_ROUNDU32_HALF;

  if (unVal < unDivide) {
    return (  (m_unVal < unVal) || (m_unVal > unDivide)  );
  } else {
    return (m_unVal > unDivide && m_unVal < unVal);
  }

}

inline bool HLH_RoundU32::operator >  (const HLH_RoundU32 &zhrVal) const
{

  UINT32  unDivide;
  UINT32  unVal;

  unVal    = zhrVal.m_unVal;
  unDivide = unVal + HLH_ROUNDU32_HALF;

  if (unVal < unDivide) {
    return ( m_unVal > unVal && m_unVal <= unDivide );
  } else {
    return ( m_unVal <= unDivide || m_unVal > unVal );
  }

}

inline bool HLH_RoundU32::operator <= (const HLH_RoundU32 &zhrVal) const
{

  UINT32  unDivide;
  UINT32  unVal;

  unVal    = zhrVal.m_unVal;
  unDivide = unVal + HLH_ROUNDU32_HALF;

  if (unVal < unDivide) {
    return (  (m_unVal <= unVal) || (m_unVal > unDivide)  );
  } else {
    return (m_unVal > unDivide && m_unVal <= unVal);
  }

}

inline bool HLH_RoundU32::operator >= (const HLH_RoundU32 &zhrVal) const
{

  UINT32  unDivide;
  UINT32  unVal;

  unVal    = zhrVal.m_unVal;
  unDivide = unVal + HLH_ROUNDU32_HALF;

  if (unVal < unDivide) {
    return ( m_unVal >= unVal && m_unVal <= unDivide );
  } else {
    return ( m_unVal <= unDivide || m_unVal >= unVal);
  }

}





#endif /* __HLH_ROUNDU32_INC_20091014_142615_HENRY__ */
