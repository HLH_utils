/*******************************************************************************
 *    File Name : HLH_Sock.cpp
 * 
 *       Author : Henry He
 * Created Time : 2009-10-8 13:56:06
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/


#include "HLH_utils/typedef.h"
#include "HLH_utils/HLH_Sock.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/




/******************************************************************************
 * Desc : Member Functions
 ******************************************************************************/





/******************************************************************************
 * Desc : Constructor / Deconstructor
 ******************************************************************************/



/******************************************************************************
 * Func : HLH_Sock::HLH_Sock
 * Desc : Constructor of HLH_Sock
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
HLH_Sock::HLH_Sock ()
{

  // Init member variables
  m_fdSocket     = -1;
  m_bCreated    = false;

  // Init Mutexs
  m_zhmMainMutex.Init ();
  m_zhmSendMutex.Init ();
  m_zhmRecvMutex.Init ();
}



/******************************************************************************
 * Func : HLH_Sock::~HLH_Sock
 * Desc : Deconstructor of HLH_Sock
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
HLH_Sock::~HLH_Sock ()
{
  Destroy ();
}



/******************************************************************************
 * Desc : Common Operations
 ******************************************************************************/





/******************************************************************************
 * Func : HLH_Sock::Create
 * Desc : Initialize the socket and bind it 
 * Args : zhsSockType         socket type
 *        zhsSockAddr         address to be binded
 *        bNonblocking        whether create this socket nonblocking
 *                            (default is blocking)
 * Outs : if success, return 0, otherwise return error code
 ******************************************************************************/
int HLH_Sock::Create (HLH_SockType zhsSockType,
    const HLH_SockAddr &zhsSockAddr, bool bNonblocking)
{
  int nRetVal;
  int nFlags;

  // Lock before process
  m_zhmMainMutex.Lock ();

  if (m_bCreated) {
    // Unlock after process
    m_zhmMainMutex.Unlock ();
		HLH_DEBUG ( HLH_DEBUG_MAIN, ("already created") );
    return HLH_SOCK_ERR_ALREADY_CREATED;
  }

  // Create socket
  switch (zhsSockType) {
    case HLH_SOCK_TYPE_STREAM:
      m_fdSocket = socket (AF_INET, SOCK_STREAM, 0);
      break;

    case HLH_SOCK_TYPE_DGRAM:
      m_fdSocket = socket (AF_INET, SOCK_DGRAM, 0);
      break;

    default:
			HLH_DEBUG ( HLH_DEBUG_UTILS, ("sock type not supported") );
      goto failed;
  }

  if (m_fdSocket < 0) {
		HLH_DEBUG ( HLH_DEBUG_MAIN, ("can't create socket") );
    goto failed;
  }

  // Set Nonblocking if request
  if (bNonblocking) {
    nFlags  = fcntl (m_fdSocket, F_GETFL, 0);
    nRetVal = fcntl (m_fdSocket, F_SETFL, nFlags|O_NONBLOCK);

    if (nRetVal < 0) {
			HLH_DEBUG ( HLH_DEBUG_MAIN, ("can't set nonblock") );
      goto failed;
    }
  }

  // Bind on specific address
  nRetVal = bind (  m_fdSocket, zhsSockAddr.GetAddrPointer (),
      zhsSockAddr.GetAddrLen ()  );

  if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_MAIN, ("can't bind on address") );
    goto failed;
  }

  // Prepare fd_set for poll
  FD_ZERO (&m_fsReadSet);
  FD_ZERO (&m_fsWriteSet);
  FD_ZERO (&m_fsErrorSet);

  FD_SET (m_fdSocket, &m_fsReadSet);
  FD_SET (m_fdSocket, &m_fsWriteSet);
  FD_SET (m_fdSocket, &m_fsErrorSet);

  // Notify that this socket has been create
  m_bCreated = true;

  // Unlock after process
  m_zhmMainMutex.Unlock ();
  return 0;

failed:

  // Close socket if failed after create
  if (m_fdSocket >= 0) {
    close (m_fdSocket);
    m_fdSocket = -1;
  }

  // Unlock after process
  m_zhmMainMutex.Unlock ();

  return HLH_SOCK_ERR_CANT_CREATE;

}


/******************************************************************************
 * Func : HLH_Sock::Create
 * Desc : Initialize the socket from a initialized socket file descriptor
 * Args : fdSock              file descriptor to create from
 * Outs : if success return 0, otherwise return error code
 ******************************************************************************/
int HLH_Sock::Create (int fdSock, bool bNonblocking)
{

  int nRetVal;
  int nFlags;

  // Lock before process
  m_zhmMainMutex.Lock ();

  if (m_bCreated) {
    // Unlock after process
    m_zhmMainMutex.Unlock ();
		HLH_DEBUG ( HLH_DEBUG_MAIN, ("already created") );
    return HLH_SOCK_ERR_ALREADY_CREATED;
  }

  m_fdSocket = fdSock;

  // Set Nonblocking if request
  if (bNonblocking) {
    nFlags  = fcntl (m_fdSocket, F_GETFL, 0);
    nRetVal = fcntl (m_fdSocket, F_SETFL, nFlags|O_NONBLOCK);

    if (nRetVal < 0) {
			HLH_DEBUG ( HLH_DEBUG_MAIN, ("can't set nonblock") );
      goto failed;
    }
  }


  // Prepare fd_set for poll
  FD_ZERO (&m_fsReadSet);
  FD_ZERO (&m_fsWriteSet);
  FD_ZERO (&m_fsErrorSet);

  FD_SET (m_fdSocket, &m_fsReadSet);
  FD_SET (m_fdSocket, &m_fsWriteSet);
  FD_SET (m_fdSocket, &m_fsErrorSet);

  // Notify that this socket has been created
  m_bCreated = true;

  // Unlock after process
  m_zhmMainMutex.Unlock ();

  return 0;

failed:

  // Unlock after process
  m_zhmMainMutex.Unlock ();

  return HLH_SOCK_ERR_CANT_CREATE;

}


/******************************************************************************
 * Func : HLH_Sock::Destroy
 * Desc : Destroy the socket, will wait for send/pollwait operations
 * Args : NONE
 * Outs : If success, return 0, otherwise return error code
 ******************************************************************************/
int HLH_Sock::Destroy ()
{

  // Wait for send operations
  m_zhmSendMutex.Lock ();

  // Wait for operations other than read/write
  m_zhmMainMutex.Lock ();

  // if not create, can't close
  if (!m_bCreated) {
    // Unlock operations other than read/write
    m_zhmMainMutex.Unlock ();

    // Unlock send operations
    m_zhmSendMutex.Unlock ();

    return HLH_SOCK_ERR_NOT_CREATED;
  }


  // Receive operations won't be waited

  // Close the socket now
  close (m_fdSocket);
  m_fdSocket = -1;
  m_bCreated = false;


  // Unlock operations other than read/write
  m_zhmMainMutex.Unlock ();

  // Unlock send operations
  m_zhmSendMutex.Unlock ();

  return 0;
}




/******************************************************************************
 * Func : HLH_Sock::IsCreated
 * Desc : Whether this socket created
 * Args : NONE
 * Outs : If created return true, otherwise return false
 ******************************************************************************/
bool HLH_Sock::IsCreated ()
{
  bool bCreated;

  m_zhmMainMutex.Lock ();
  bCreated = m_bCreated;
  m_zhmMainMutex.Unlock ();

  return bCreated;
}




/******************************************************************************
 * Func : HLH_Sock::Listen
 * Desc : Listen for socket connections and limit the queue of incoming connections   
 * Args : unQueue           length of listen queue
 * Outs : if success, return 0, otherwise return error code
 ******************************************************************************/
int HLH_Sock::Listen (UINT32 unQueue)
{
  int nRetVal;

  // Lock before process
  m_zhmMainMutex.Lock ();

  // Can't listen on socket which is not created
  if (!m_bCreated) {
    // Unlock after process
    m_zhmMainMutex.Unlock ();

		HLH_DEBUG ( HLH_DEBUG_UTILS, ("not created") );
    return HLH_SOCK_ERR_NOT_CREATED;
  }

  nRetVal = listen (m_fdSocket, unQueue);
  if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_UTILS, ("listen failed") );
    goto failed;
  }

  // Unlock after process
  m_zhmMainMutex.Unlock ();

  return 0;

failed:
  // Unlock after process
  m_zhmMainMutex.Unlock ();

  return HLH_SOCK_ERR_FAILED;
}


/******************************************************************************
 * Func : HLH_Sock::Connect
 * Desc : Requests a connection to be made on a socket  
 * Args : zhsPeekAddr             address to connect
 * Outs : if success, return 0, otherwise return error code
 ******************************************************************************/
int HLH_Sock::Connect (const HLH_SockAddr &zhsPeekAddr)
{
  int nRetVal;

  // Lock before process
  m_zhmMainMutex.Lock ();

  // Can't Connect on socket which is not created
  if (!m_bCreated) {
    // Unlock after process
    m_zhmMainMutex.Unlock ();

		HLH_DEBUG ( HLH_DEBUG_UTILS, ("not created") );
    return HLH_SOCK_ERR_NOT_CREATED;
  }

  // Connect to specific address
  nRetVal = connect (  m_fdSocket, zhsPeekAddr.GetAddrPointer (),
      zhsPeekAddr.GetAddrLen ()  );
  if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_UTILS, ("connect failed") );
    goto failed;
  }

  // Unlock after process
  m_zhmMainMutex.Unlock ();

  return 0;

failed:
  // Unlock after process
  m_zhmMainMutex.Unlock ();

  return HLH_SOCK_ERR_FAILED;
}


/******************************************************************************
 * Func : HLH_Sock::Accept
 * Desc : Extracts the first connection on the queue of pending connections,
 *        creates a new socket with the same socket type protocol and address family as the specified socket,
 *        and allocates a new file descriptor for that socket 
 * Args : zhsPeerAddr             address of peer comming in
 * Outs : if success, return 0, otherwise return error code
 ******************************************************************************/
int HLH_Sock::Accept (HLH_SockAddr &zhsPeerAddr)
{
  int                 nRetVal;
  socklen_t           slLen;

  // Lock before process
  m_zhmMainMutex.Lock ();

  // Can't Accept on socket which is not created
  if (!m_bCreated) {
    // Unlock after process
    m_zhmMainMutex.Unlock ();

		HLH_DEBUG ( HLH_DEBUG_UTILS, ("not created") );
    return HLH_SOCK_ERR_NOT_CREATED;
  }

  // Wait and accept connections
  slLen = zhsPeerAddr.GetAddrLen ();
  nRetVal = accept ( m_fdSocket, zhsPeerAddr.GetAddrPointer(), &slLen );
  if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_UTILS, ("accept failed") );
    goto failed;
  }

  // Unlock after process
  m_zhmMainMutex.Unlock ();

  return nRetVal;

failed:
  // Unlock after process
  m_zhmMainMutex.Unlock ();

  return HLH_SOCK_ERR_FAILED;
}


/******************************************************************************
 * Func : HLH_Sock::Send
 * Desc : Initiates transmission of a message from the specified socket to its peer 
 * Args : pvBuf         pointer of buffer
 *        unLen         length of buffer
 *        unFlags       flags used by send ()
 * Outs : if success return the length of data send, otherwise error code
 ******************************************************************************/
int HLH_Sock::Send (const void *pvBuf, UINT32 unLen, UINT32 unFlags)
{
  int nRetVal;

  // Lock the send mutex
  m_zhmSendMutex.Lock ();

  // Lock before process
  m_zhmMainMutex.Lock ();

  // Can't send on socket not created
  if (!m_bCreated) {

    // Unlock after process
    m_zhmMainMutex.Unlock ();

    // Unlock send mutex
    m_zhmSendMutex.Unlock ();

		HLH_DEBUG ( HLH_DEBUG_UTILS, ("not created") );
    return HLH_SOCK_ERR_NOT_CREATED;
  }

  // Unlock after process
  m_zhmMainMutex.Unlock ();

  // Send the data now
  nRetVal = send (m_fdSocket, pvBuf, unLen, unFlags/*|MSG_NOSIGNAL*/);

  // Unlock send mutex after send
  m_zhmSendMutex.Unlock ();

  if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_UTILS, ("send failed") );
    goto failed;
  }

  return nRetVal;
failed:
  // Unlock send mutex
  m_zhmSendMutex.Unlock ();

  return HLH_SOCK_ERR_FAILED;
}


/******************************************************************************
 * Func : HLH_Sock::Recv
 * Desc : Receives a message from a connection-mode or connectionless-mode socket 
 * Args : pvBuf             pointer to buffer
 *        unLen             length of buffer
 *        unFlags           flags used by recv ()
 * Outs : if success, return length of data received, otherwise return error code
 ******************************************************************************/
int HLH_Sock::Recv (void *pvBuf, UINT32 unLen, UINT32 unFlags)
{
  int nRetVal;

  // Lock before Receiving
  m_zhmRecvMutex.Lock ();

  // Lock before process
  m_zhmMainMutex.Lock ();
  if (!m_bCreated) {

    // Unlock receive mutex
    m_zhmRecvMutex.Unlock ();

    // Unlock after process
    m_zhmMainMutex.Unlock ();

		HLH_DEBUG ( HLH_DEBUG_UTILS, ("not created") );
    return HLH_SOCK_ERR_NOT_CREATED;
  }

  // Unlock after process
  m_zhmMainMutex.Unlock ();

  nRetVal = recv (m_fdSocket, pvBuf, unLen, unFlags/*|MSG_NOSIGNAL*/);

  // Unlock after receiving
  m_zhmRecvMutex.Unlock ();

  if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_UTILS, ("receive failed") );
    goto failed;
  }

  return nRetVal;

failed:
  return HLH_SOCK_ERR_FAILED;

}


/******************************************************************************
 * Func : HLH_Sock::SendTo
 * Desc : Sends a message through a connection-mode or connectionless-mode socket 
 * Args : pvBuf         pointer of buffer
 *        unLen         length of buffer
 *        zhsSockAddr   address of peer
 *        unFlags       flags used by send ()
 * Outs : if success return the length of data send, otherwise error code
 ******************************************************************************/
int HLH_Sock::SendTo (const void *pvBuf, UINT32 unLen,
    const HLH_SockAddr &zhsSockAddr, UINT32 unFlags)
{
  int nRetVal;

  // Lock the send mutex
  m_zhmSendMutex.Lock ();

  // Lock before process
  m_zhmMainMutex.Lock ();

  if (!m_bCreated) {
    // Unlock after process
    m_zhmMainMutex.Unlock ();

    // Unlock send mutex
    m_zhmSendMutex.Unlock ();

		HLH_DEBUG ( HLH_DEBUG_UTILS, ("not created") );
    return HLH_SOCK_ERR_NOT_CREATED;
  }

  // Unlock after process
  m_zhmMainMutex.Unlock ();

  // Send the data now
  nRetVal = sendto ( m_fdSocket, pvBuf, unLen, unFlags,
      zhsSockAddr.GetAddrPointer (), zhsSockAddr.GetAddrLen () );

  // Unlock send mutex after send
  m_zhmSendMutex.Unlock ();

  if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_UTILS, ("sendto failed") );
    goto failed;
  }

  return nRetVal;

failed:
  return HLH_SOCK_ERR_FAILED;

}


/******************************************************************************
 * Func : HLH_Sock::RecvFrom
 * Desc : Receives a message from a connection-mode or connectionless-mode socket 
 * Args : pvBuf             pointer to buffer
 *        unLen             length of buffer
 *        zhsPeerAddr       used to store peer address
 *        unFlags           flags used by recv ()
 * Outs : if success, return length of data received, otherwise return error code
 ******************************************************************************/
int HLH_Sock::RecvFrom (void *pvBuf, UINT32 unLen,
    HLH_SockAddr &zhsPeekAddr, UINT32 unFlags)
{
  int           nRetVal;
  socklen_t     slLen;

  // Lock before Receiving
  m_zhmRecvMutex.Lock ();

  // Lock before process
  m_zhmMainMutex.Lock ();
  if (!m_bCreated) {
    // Unlock after process
    m_zhmMainMutex.Unlock ();

    // Unlock after Receiving
    m_zhmRecvMutex.Unlock ();

		HLH_DEBUG ( HLH_DEBUG_UTILS, ("not created") );
    return HLH_SOCK_ERR_NOT_CREATED;
  }

  // Unlock after process
  m_zhmMainMutex.Unlock ();

  // Send the data now
  slLen = zhsPeekAddr.GetAddrLen ();
  nRetVal = recvfrom (m_fdSocket, pvBuf, unLen, unFlags,
      zhsPeekAddr.GetAddrPointer (), &slLen);

  // Unlock after receiving
  m_zhmRecvMutex.Unlock ();

  // Format return value
  if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_UTILS, ("recvfrom failed") );
    goto failed;
  }

  return nRetVal;

failed:
  return HLH_SOCK_ERR_FAILED;

}





/******************************************************************************
 * Desc : Poll Operations
 ******************************************************************************/




/******************************************************************************
 * Func : HLH_Sock::Poll
 * Desc : Poll for specific events 
 * Args : unPollType            poll type
 * Outs : if success return number of events, otherwise return error code
 ******************************************************************************/
int HLH_Sock::Poll (UINT32 &unPollType)
{
  fd_set    fsReadSet;
  fd_set    fsWriteSet;
  fd_set    fsErrorSet;

  int       nRetVal;
  UINT32    unPollTypeNew;

  // Lock before process
  m_zhmMainMutex.Lock ();

  if (!m_bCreated) {
    // Unlock after process
    m_zhmMainMutex.Unlock ();

		HLH_DEBUG ( HLH_DEBUG_UTILS, ("not created") );
    return HLH_SOCK_ERR_NOT_CREATED;
  }

  memcpy ( &fsReadSet,  &m_fsReadSet,  sizeof (fd_set) );
  memcpy ( &fsWriteSet, &m_fsWriteSet, sizeof (fd_set) );
  memcpy ( &fsErrorSet, &m_fsErrorSet, sizeof (fd_set) );

  // Unlock after process,
  // Unlock before poll as we may want to close before poll successed
  m_zhmMainMutex.Unlock ();

  // Poll for specific event
  nRetVal = select ( m_fdSocket + 1,
      unPollType & HLH_SOCK_POLL_READ  ? &fsReadSet  : NULL,
      unPollType & HLH_SOCK_POLL_WRITE ? &fsWriteSet : NULL,
      unPollType & HLH_SOCK_POLL_ERROR ? &fsErrorSet : NULL,
      NULL
      );

  if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_UTILS, ("select failed") );
    goto failed;
  }

  // Set return value

  unPollTypeNew = 0;

  if (  ( unPollType & HLH_SOCK_POLL_READ )
      && FD_ISSET (m_fdSocket, &fsReadSet)  ) {
    unPollTypeNew |= HLH_SOCK_POLL_READ;
  }

  if (  ( unPollType & HLH_SOCK_POLL_WRITE )
      && FD_ISSET (m_fdSocket, &fsWriteSet)  ) {
    unPollTypeNew |= HLH_SOCK_POLL_WRITE;
  }

  if (  ( unPollType & HLH_SOCK_POLL_ERROR )
      && FD_ISSET (m_fdSocket, &fsErrorSet)  ) {
    unPollTypeNew |= HLH_SOCK_POLL_ERROR;
  }

  unPollType = unPollTypeNew;

  return nRetVal;

failed:

  return HLH_SOCK_ERR_FAILED;
}


/******************************************************************************
 * Func : HLH_Sock::PollWait
 * Desc : Poll for specific events until time eclipsed 
 * Args : unPollType            poll type
 *        zhtTime               time to wait before poll success
 * Outs : if success return number of events, otherwise return error code
 *        if failed, the time left will return in zhtTime
 ******************************************************************************/
int HLH_Sock::PollWait (UINT32 &unPollType, HLH_Time &zhtTime)
{
  fd_set          fsReadSet;
  fd_set          fsWriteSet;
  fd_set          fsErrorSet;
                 
  int             nRetVal;
  UINT32          unPollTypeNew;

  struct timeval  tvTime;

  // Lock before process
  m_zhmMainMutex.Lock ();

  if (!m_bCreated) {
    // Unlock after process
    m_zhmMainMutex.Unlock ();

		HLH_DEBUG ( HLH_DEBUG_UTILS, ("not created") );
    return HLH_SOCK_ERR_NOT_CREATED;
  }

  memcpy ( &fsReadSet,  &m_fsReadSet,  sizeof (fd_set) );
  memcpy ( &fsWriteSet, &m_fsWriteSet, sizeof (fd_set) );
  memcpy ( &fsErrorSet, &m_fsErrorSet, sizeof (fd_set) );

  // Poll for specific event
  tvTime = zhtTime.GetTimeVal ();

  // Unlock after process
  m_zhmMainMutex.Unlock ();

  nRetVal = select ( m_fdSocket + 1,
      unPollType & HLH_SOCK_POLL_READ  ? &fsReadSet  : NULL,
      unPollType & HLH_SOCK_POLL_WRITE ? &fsWriteSet : NULL,
      unPollType & HLH_SOCK_POLL_ERROR ? &fsErrorSet : NULL,
      &tvTime
      );

  if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_UTILS, ("select failed") );
    goto failed;
  }

  // Return the time left
  zhtTime.SetTime (tvTime);

  // Set return value

  unPollTypeNew = 0;

  if (  ( unPollType & HLH_SOCK_POLL_READ )
      && FD_ISSET (m_fdSocket, &fsReadSet)  ) {
    unPollTypeNew |= HLH_SOCK_POLL_READ;
  }

  if (  ( unPollType & HLH_SOCK_POLL_WRITE )
      && FD_ISSET (m_fdSocket, &fsWriteSet)  ) {
    unPollTypeNew |= HLH_SOCK_POLL_WRITE;
  }

  if (  ( unPollType & HLH_SOCK_POLL_ERROR )
      && FD_ISSET (m_fdSocket, &fsErrorSet)  ) {
    unPollTypeNew |= HLH_SOCK_POLL_ERROR;
  }

  unPollType = unPollTypeNew;

  return nRetVal;

failed:
  return HLH_SOCK_ERR_FAILED;

}




