/*******************************************************************************
 *         File : HLH_Sock.h
 * 
 *       Author : Henry He
 *      Created : 2009-10-8 13:55:48
 *  Description : 
 ******************************************************************************/

#ifndef __HLH_SOCK_INC_20091008_135548_HENRY__
#define __HLH_SOCK_INC_20091008_135548_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/

#include "HLH_utils/typedef.h"
#include "HLH_utils/HLH_Time.h"
#include "HLH_utils/HLH_Thread.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


////////////////////////////////////////////////////////////////////////////////
// Poll Types

#define HLH_SOCK_POLL_READ                              0x01
#define HLH_SOCK_POLL_WRITE                             0x02
#define HLH_SOCK_POLL_ERROR                             0x04

////////////////////////////////////////////////////////////////////////////////
// Error Types

#define HLH_SOCK_ERR_FAILED                             (-1)
#define HLH_SOCK_ERR_ALREADY_CREATED                    (-2)
#define HLH_SOCK_ERR_CANT_CREATE                        (-3)
#define HLH_SOCK_ERR_NOT_CREATED                        (-4)

////////////////////////////////////////////////////////////////////////////////
// Default Values

#define HLH_SOCK_DEFAULT_LISTEN_QUEUE_NUM               10


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Socket type

typedef enum HLH_SockType
{
	HLH_SOCK_TYPE_STREAM = 0x01,
	HLH_SOCK_TYPE_DGRAM  = 0x02
} HLH_SockType;



/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Classes
 ******************************************************************************/




////////////////////////////////////////////////////////////////////////////////
// Socket Address : simple wrapper of sockaddr_t

class HLH_SockAddr
{
	friend class HLH_Sock;

	public:

	/******************************************************************************
	 * Desc : Constructor / Deconstructor
	 ******************************************************************************/

	HLH_SockAddr () {
		m_saAddr.sin_addr.s_addr = htonl ( INADDR_ANY );
		m_saAddr.sin_port        = htons ( rand() & 0xFFFF );
		m_saAddr.sin_family      = AF_INET;
	}

	HLH_SockAddr (UINT32 unIP, UINT16 usPort, sa_family_t sfFamily = AF_INET) {
		m_saAddr.sin_addr.s_addr = htonl ( unIP );
		m_saAddr.sin_port        = htons ( usPort );
		m_saAddr.sin_family      = sfFamily;
	}

	HLH_SockAddr (const char *pcIP, UINT16 usPort, sa_family_t sfFamily = AF_INET) {
		m_saAddr.sin_addr.s_addr = inet_addr ( pcIP );
		m_saAddr.sin_port        = htons ( usPort );
		m_saAddr.sin_family      = sfFamily;
	}

	~HLH_SockAddr () { }

	/******************************************************************************
	 * Desc : Operations
	 ******************************************************************************/

	struct sockaddr * GetAddrPointer () const  { return (struct sockaddr *)(&m_saAddr); }

	UINT32 GetAddrLen  () const                { return sizeof (m_saAddr); }

	void SetAddr (struct sockaddr_in &saAddr) {
		m_saAddr = saAddr;
	}

	void SetAddr (UINT32 unIP, UINT16 usPort, sa_family_t sfFamily = AF_INET) {
		m_saAddr.sin_addr.s_addr = htonl ( unIP );
		m_saAddr.sin_port        = htons ( usPort );
		m_saAddr.sin_family      = sfFamily;
	}

	void SetAddr (const char *pcIP, UINT16 usPort, sa_family_t sfFamily = AF_INET) {
		m_saAddr.sin_addr.s_addr = inet_addr (pcIP);
		m_saAddr.sin_port        = htons ( usPort );
		m_saAddr.sin_family      = sfFamily;
	}

	void SetAddrFamily (sa_family_t sfFamily) {
		m_saAddr.sin_family      = sfFamily;
	}

	void SetAddrPort (UINT16 usPort) {
		m_saAddr.sin_port        = htons ( usPort );
	}

	void SetAddrIP (UINT32 unIP) {
		m_saAddr.sin_addr.s_addr = htonl ( unIP );
	}

	int SetAddrIP (const char *pcIP) {
    in_addr_t s_addr;

		s_addr = inet_addr (pcIP);
    if ( s_addr == (in_addr_t)(-1) ) {
      return -1;
    }
		m_saAddr.sin_addr.s_addr = inet_addr (pcIP);
    return 0;
	}

  
  bool operator == (const HLH_SockAddr &zhsAddr) const {
    return ( m_saAddr.sin_addr.s_addr == zhsAddr.m_saAddr.sin_addr.s_addr
        &&   m_saAddr.sin_port        == zhsAddr.m_saAddr.sin_port
        &&   m_saAddr.sin_family      == zhsAddr.m_saAddr.sin_family );
  }

	private:
	struct sockaddr_in m_saAddr;
};



////////////////////////////////////////////////////////////////////////////////
// Class of HLH_Sock: simple wrapper of socket funtions

class HLH_Sock
{

	public:
		/******************************************************************************
		 * Desc : Constructor / Deconstructor
		 ******************************************************************************/
		HLH_Sock ();
		~HLH_Sock ();

	public:
		/******************************************************************************
		 * Desc : Common Operations
		 ******************************************************************************/

		// Initialize the socket and bind it
		int Create (HLH_SockType zhsSockType,
				const HLH_SockAddr &zhsSockAddr, bool bNonblocking = false);

		// Initialize the socket from a initialized socket
		int Create (int fdSock, bool bNonblocking = false);

		// Whether this socket created
		bool IsCreated ();

		// Listen for socket connections and limit the queue of incoming connections  
		int Listen (UINT32 unQueue = HLH_SOCK_DEFAULT_LISTEN_QUEUE_NUM);

		// Requests a connection to be made on a socket 
		int Connect (const HLH_SockAddr &zhsSockAddr);

		// Extracts the first connection on the queue of pending connections,
		// creates a new socket with the same socket type protocol and address family as the specified socket,
		// and allocates a new file descriptor for that socket
		int Accept (HLH_SockAddr &zhsSockAddr);

		// Initiates transmission of a message from the specified socket to its peer
		int Send (const void *pvBuf, UINT32 unLen, UINT32 unFlags = 0);

		// Receives a message from a connection-mode or connectionless-mode socket
		int Recv (void *pvBuf, UINT32 unLen, UINT32 unFlags = 0);

		// Sends a message through a connection-mode or connectionless-mode socket
		int SendTo (const void *pvBuf, UINT32 unLen,
				const HLH_SockAddr &zhsSockAddr, UINT32 unFlags);

		// Receives a message from a connection-mode or connectionless-mode socket
		int RecvFrom (void *pvBuf, UINT32 unLen,
				HLH_SockAddr &zhsSockAddr, UINT32 unFlags = 0);

		// Destroy the socket (wait until send/pollwait operations finished)
		int Destroy ();




		/******************************************************************************
		 * Desc : Poll Operations
		 ******************************************************************************/

		// Poll for specific events
		int Poll (UINT32 &unPollType);

		// Poll for specific events until time eclipsed
		int PollWait (UINT32 &unPollType, HLH_Time &zhtTime);


	private:
		/******************************************************************************
		 * Desc : Private Data
		 ******************************************************************************/

		// socket file descriptor
		int             m_fdSocket;

		// Whether this socket is created
		bool            m_bCreated;


		// Master Lock of HLH_Sock
		HLH_Mutex       m_zhmMainMutex;

		// Read Lock
		HLH_Mutex       m_zhmRecvMutex;

		// Write Lock
		HLH_Mutex       m_zhmSendMutex;


		// Poll file discriptor set: initialized when created
		fd_set          m_fsReadSet;
		fd_set          m_fsWriteSet;
		fd_set          m_fsErrorSet;

};






#endif /* __HLH_SOCK_INC_20091008_135548_HENRY__ */
