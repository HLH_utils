/*******************************************************************************
 *         File : HLH_Time.h
 * 
 *       Author : Henry He
 *      Created : 2009-10-8 10:22:07
 *  Description : 
 ******************************************************************************/

#ifndef __HLHTIME_INC_20091008_102207_HENRY__
#define __HLHTIME_INC_20091008_102207_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include "HLH_utils/typedef.h"
#include "HLH_utils/HLH_RoundU32.h"




/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/
#define HLH_NTPTIMEOFFSET                 2208988800UL


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Functions
 ******************************************************************************/



/******************************************************************************
 * Desc : 
 *    This is a simple wrapper for the most significant word (MSW) and least 
 *    significant word (LSW) of an NTP timestamp.
 ******************************************************************************/
class HLH_NTPTime
{

  public:
    /** This constructor creates and instance with MSW \c m and LSW \c l. */
    HLH_NTPTime(UINT32 unMsw, UINT32 unLsw) {
      m_unMsw = unMsw;
      m_unLsw = unLsw;
    }

    /** Returns the most significant word. */
    UINT32 GetMSW () const {
      return m_unMsw;
    }

    /** Returns the least significant word. */
    UINT32 GetLSW () const {
      return m_unLsw;
    }

  private:
    UINT32    m_unMsw;
    UINT32    m_unLsw;

};



/******************************************************************************
 * Desc : 
 *    This class is used to specify wallclock time, delay intervals etc.
 *    It stores a number of seconds and a number of microseconds.
 ******************************************************************************/
class HLH_Time
{

  public:
    /** Creates an HLH_Time instance representing \c dTime,
     *  which is expressed in units of seconds.
     */
    HLH_Time (double dTime) {
      SetTime (dTime);
    }

    /** Creates an instance that corresponds to \c zhnTime. 
     *  If the conversion cannot be made, both the seconds and the
     *  microseconds are set to zero.
     */
    HLH_Time (const HLH_NTPTime &zhnTime) {
      SetTime (zhnTime);
    }

    /** Creates an instance corresponding to \c seconds and \c microseconds. */
    HLH_Time (UINT32 unSecs = 0, UINT32 unMicroSecs = 0)  {
      SetTime (unSecs, unMicroSecs);
    }

    /** Creates an instance corresponding to \c tvTime. */
    HLH_Time (const struct timeval &tvTime) {
      SetTime (tvTime);
    }


  public:

    // Set time to dTime
    void SetTime (double dTime);

    // Set time to zhnTime
    void SetTime (const HLH_NTPTime &zhnTime);

    // Set time to (unSecs, unMicroSecs)
    void SetTime (UINT32 unSecs, UINT32 unMicroSecs) {
      m_zhrSecs     = unSecs;
      m_unMicroSecs = unMicroSecs;
    }

    // Set the time to tvTime
    void SetTime (const struct timeval &tvTime);


  public:

    /** Returns the number of seconds stored in this instance. */
    UINT32 GetSeconds () const          { return (UINT32)m_zhrSecs; }

    /** Returns the number of microseconds stored in this instance. */
    UINT32 GetMicroSeconds () const     { return m_unMicroSecs; }

    /** Returns the time stored in this instance, expressed in units of seconds. */
    double GetDouble () const           { return ( ((double)m_zhrSecs) + (((double)m_unMicroSecs)/1000000.0) ); }

    /** Returns the NTP time corresponding to the time stored in this instance. */
    HLH_NTPTime GetNTPTime () const;

    /** Returns the timeval corresponding to the time stored in this instance. */
    struct timeval GetTimeVal () const;


  public:
    /** Returns an HLH_Time instance representing the current wallclock time. 
     *  This is expressed as a number of seconds since 00:00:00 UTC, January 1, 1970.
     */
    static HLH_Time GetCurrentTime ();

    /** This function waits the amount of time specified in \c delay. */
    static void WaitX (HLH_Time &zhtDelay);
    static void Wait (HLH_Time zhtDelay) { WaitX (zhtDelay); }



  public:

    HLH_Time operator + (const HLH_Time &zhtTime) const;
    HLH_Time operator - (const HLH_Time &zhtTime) const;
    HLH_Time operator * (UINT32 unVal) const;
    HLH_Time operator / (UINT32 unVal) const;

    HLH_Time &operator += (const HLH_Time &zhtTime);
    HLH_Time &operator -= (const HLH_Time &zhtTime);
    HLH_Time &operator *= (UINT32 unVal);
    HLH_Time &operator /= (UINT32 unVal);

    bool operator <  (const HLH_Time &zhtTime) const;
    bool operator >  (const HLH_Time &zhtTime) const;
    bool operator <= (const HLH_Time &zhtTime) const;
    bool operator >= (const HLH_Time &zhtTime) const;

  private:
    HLH_RoundU32    m_zhrSecs;
    UINT32          m_unMicroSecs;

};





/******************************************************************************
 * Desc : Member functions
 ******************************************************************************/




/******************************************************************************
 * Func : HLH_Time::SetTime
 * Desc : Set time to dTime
 * Args : dTime               Time to be set
 * Outs : NONE
 ******************************************************************************/
inline void HLH_Time::SetTime (double dTime)
{
  double dMicroSecs;

  m_zhrSecs     = (UINT32)dTime;
  dMicroSecs    = ( dTime - ((double)m_zhrSecs) ) * 1000000.0;
  m_unMicroSecs = (UINT32) dMicroSecs;
}


/******************************************************************************
 * Func : HLH_Time::SetTime
 * Desc : Set time to zhnTime
 * Args : zhnTime       Time to be set
 * Outs : NONE
 ******************************************************************************/
inline void HLH_Time::SetTime (const HLH_NTPTime &zhnTime)
{
  double dMicroSecs;

  if (zhnTime.GetMSW() < HLH_NTPTIMEOFFSET) {

    m_zhrSecs = 0;
    m_unMicroSecs = 0;

  } else {

    m_zhrSecs     = zhnTime.GetMSW () - HLH_NTPTIMEOFFSET;
    dMicroSecs    = (double) zhnTime.GetLSW ();
    dMicroSecs   /= ( 65536.0 * 65536.0 );
    dMicroSecs   *= 1000000.0;
    m_unMicroSecs = (UINT32) dMicroSecs;

  }
}



/******************************************************************************
 * Func : HLH_Time::SetTime
 * Desc : Set the time to tvTime 
 * Args : tvTime          Time to be set
 * Outs : NONE
 ******************************************************************************/
inline void HLH_Time::SetTime (const struct timeval &tvTime)
{
  m_zhrSecs     = tvTime.tv_sec;
  m_unMicroSecs = tvTime.tv_usec;
}



/******************************************************************************
 * Func : HLH_Time::GetNTPTime
 * Desc : Get time in NTP format
 * Args : NONE
 * Outs : return time in NTP format
 ******************************************************************************/
inline HLH_NTPTime HLH_Time::GetNTPTime () const
{
  UINT32 unMsw;
  UINT32 unLsw;
  double dLsw;

  unMsw = (UINT32)m_zhrSecs + HLH_NTPTIMEOFFSET;
  dLsw  = m_unMicroSecs / 1000000.0;
  dLsw *= (65536.0 * 65536.0);
  unLsw = (UINT32) dLsw;

  return HLH_NTPTime (unMsw, unLsw);
}


/******************************************************************************
 * Func : HLH_Time::GetTimeVal
 * Desc : Returns the timeval corresponding to the time stored in this instance.
 * Args : NONE
 * Outs : Returns the timeval corresponding to the time stored in this instance.
 ******************************************************************************/
inline struct timeval HLH_Time::GetTimeVal () const
{
  struct timeval tvTime;

  tvTime.tv_sec  = (UINT32) m_zhrSecs;
  tvTime.tv_usec = m_unMicroSecs;

  return tvTime;
}






/******************************************************************************
 * Func : GetCurrentTime
 * Desc : Get current time
 * Args : NONE
 * Outs : Return current time
 ******************************************************************************/
inline HLH_Time HLH_Time::GetCurrentTime ()
{
  struct timeval tvTime;

  gettimeofday (&tvTime, 0);

  return  HLH_Time (tvTime);
}

/******************************************************************************
 * Func : Wait
 * Desc : Wait for 'zhtDelay' time
 * Args : zhtDelay      Time to wait
 * Outs : NONE
 ******************************************************************************/
inline void HLH_Time::WaitX (HLH_Time &zhtDelay)
{
  struct timespec tsReq;
  struct timespec tsRem;

  tsReq.tv_sec  = (UINT32) zhtDelay.m_zhrSecs;
  tsReq.tv_nsec = zhtDelay.m_unMicroSecs * 1000;

  // Sleep for tsReq
  nanosleep ( &tsReq, &tsRem );

  // Set remained time
  zhtDelay.SetTime ( tsRem.tv_sec, tsRem.tv_nsec / 1000 );
}







/******************************************************************************
 * Desc : Operators
 ******************************************************************************/


inline HLH_Time HLH_Time::operator + (const HLH_Time &zhtTime) const
{
  HLH_Time    zhtResult;

  zhtResult.m_zhrSecs     = m_zhrSecs + zhtTime.m_zhrSecs;
  zhtResult.m_unMicroSecs = m_unMicroSecs + zhtTime.m_unMicroSecs;

  if (zhtResult.m_unMicroSecs > 1000000) {
    zhtResult.m_unMicroSecs -= 1000000;
    zhtResult.m_zhrSecs ++;
  }

  return  zhtResult;
}


inline HLH_Time HLH_Time::operator - (const HLH_Time &zhtTime) const
{
  HLH_Time    zhtResult;

  zhtResult.m_zhrSecs     = m_zhrSecs - zhtTime.m_zhrSecs;
  zhtResult.m_unMicroSecs = m_unMicroSecs;

  if (zhtResult.m_unMicroSecs < zhtTime.m_unMicroSecs)
  {
    zhtResult.m_zhrSecs --;
    zhtResult.m_unMicroSecs += 1000000;
  }
  zhtResult.m_unMicroSecs -= zhtTime.m_unMicroSecs;

  return zhtResult;
}

inline HLH_Time HLH_Time::operator * (UINT32 unVal) const
{
  HLH_Time        zhtResult;
  UINT32          unMicroSecs;

  unMicroSecs             = m_unMicroSecs * unVal;

  zhtResult.m_zhrSecs     = ((UINT32)m_zhrSecs) * unVal + unMicroSecs / 1000000;
  zhtResult.m_unMicroSecs = unMicroSecs % 1000000;

  return  zhtResult;
}


inline HLH_Time HLH_Time::operator / (UINT32 unVal) const
{
  HLH_Time        zhtResult;
  HLH_RoundU32    zhrSecs;
  UINT32          unMicroSecs;

  zhtResult.m_zhrSecs     = (UINT32)m_zhrSecs / unVal;

  unMicroSecs             = ( (UINT32)m_zhrSecs % unVal ) * 1000000 + m_unMicroSecs;
  zhtResult.m_unMicroSecs = unMicroSecs / unVal;

  return  zhtResult;
}



inline HLH_Time & HLH_Time::operator += (const HLH_Time &zhtTime)
{ 
  m_zhrSecs     += zhtTime.m_zhrSecs; 
  m_unMicroSecs += zhtTime.m_unMicroSecs;
  if (m_unMicroSecs >= 1000000) {
    m_zhrSecs ++;
    m_unMicroSecs -= 1000000;
  }

  return *this;
}


inline HLH_Time & HLH_Time::operator -= (const HLH_Time &zhtTime)
{
  m_zhrSecs -= zhtTime.m_zhrSecs;
  if (zhtTime.m_unMicroSecs > m_unMicroSecs) {
    m_zhrSecs--;
    m_unMicroSecs += 1000000;
  }
  m_unMicroSecs -= zhtTime.m_unMicroSecs;

  return *this;
}


inline HLH_Time & HLH_Time::operator *= (UINT32 unVal)
{
  UINT32          unMicroSecs;

  unMicroSecs    = m_unMicroSecs * unVal;
  m_zhrSecs      = (UINT32)m_zhrSecs * unVal + unMicroSecs / 1000000;
  m_unMicroSecs  = unMicroSecs % 1000000;

  return  *this;
}


inline HLH_Time & HLH_Time::operator /= (UINT32 unVal)
{
  HLH_RoundU32    zhrSecs;
  UINT32          unMicroSecs;


  m_zhrSecs    /= unVal;

  unMicroSecs   = ( (UINT32)m_zhrSecs % unVal) * 1000000 + m_unMicroSecs;
  m_unMicroSecs = unMicroSecs / unVal;

  return  *this;
}





inline bool HLH_Time::operator < (const HLH_Time &zhtTime) const
{
  if (m_zhrSecs < zhtTime.m_zhrSecs)
    return true;
  if (m_zhrSecs > zhtTime.m_zhrSecs)
    return false;
  if (m_unMicroSecs < zhtTime.m_unMicroSecs)
    return true;
  return false;
}

inline bool HLH_Time::operator > (const HLH_Time &zhtTime) const
{
  if (m_zhrSecs > zhtTime.m_zhrSecs)
    return true;
  if (m_zhrSecs < zhtTime.m_zhrSecs)
    return false;
  if (m_unMicroSecs > zhtTime.m_unMicroSecs)
    return true;
  return false;
}

inline bool HLH_Time::operator <= (const HLH_Time &zhtTime) const
{
  if (m_zhrSecs < zhtTime.m_zhrSecs)
    return true;
  if (m_zhrSecs > zhtTime.m_zhrSecs)
    return false;
  if (m_unMicroSecs <= zhtTime.m_unMicroSecs)
    return true;
  return false;
}


inline bool HLH_Time::operator >= (const HLH_Time &zhtTime) const
{
  if (m_zhrSecs > zhtTime.m_zhrSecs)
    return true;
  if (m_zhrSecs < zhtTime.m_zhrSecs)
    return false;
  if (m_unMicroSecs >= zhtTime.m_unMicroSecs)
    return true;
  return false;
}





#endif /* __HLHTIME_INC_20091008_102207_HENRY__ */
