/*******************************************************************************
 *         File : utils\common.h
 * 
 *       Author : Henry He
 *      Created : 2009-10-21 13:15:05
 *  Description : 
 ******************************************************************************/

#ifndef __COMMON_INC_20091021_131505_HENRY__
#define __COMMON_INC_20091021_131505_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/



#include "HLH_utils/typedef.h"

#include "HLH_utils/HLH_RoundU32.h"
#include "HLH_utils/HLH_Time.h"

#include "HLH_utils/HLH_Mutex.h"
#include "HLH_utils/HLH_Cond.h"
#include "HLH_utils/HLH_Thread.h"

#include "HLH_utils/HLH_Sock.h"
#include "HLH_utils/HLH_UDPSock.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Functions
 ******************************************************************************/


#endif /* __COMMON_INC_20091021_131505_HENRY__ */
