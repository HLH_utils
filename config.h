/*******************************************************************************
 *         File : config.h
 * 
 *       Author : Henry He
 *      Created : Tue 27 Oct 2009 01:45:02 PM CST
 *  Description : 
 ******************************************************************************/

#ifndef __CONFIG_INC_20091027_134502_HENRY__
#define __CONFIG_INC_20091027_134502_HENRY__

/*******************************************************************************
 * Desc : Debug Configurations
 ******************************************************************************/


//===========================  Global Debug Options  ===========================

// Whether use \c ASSERT
#define __USE_ASSERT__              1

// Whether use \c HLH_DEBUG
#define __USE_HLH_DEBUG__           1

// Whether use \c HLH_ENTER_FUNC and \c HLH_EXIT_FUNC
#define __TRACE_CALL__              1




//========================  Debug Options of Modules  ==========================
// 
// Set config to '1' if enable
//

#define HLH_DEBUG_CONFIG_AUDIO      0
#define HLH_DEBUG_CONFIG_LUDP       0
#define HLH_DEBUG_CONFIG_JITTER     0
#define HLH_DEBUG_CONFIG_UTILS      0
#define HLH_DEBUG_CONFIG_VIDEO      0
#define HLH_DEBUG_CONFIG_VPHONE     1
#define HLH_DEBUG_CONFIG_FB         0
#define HLH_DEBUG_CONFIG_PP         0
#define HLH_DEBUG_CONFIG_MFC        0

#define HLH_DEBUG_CONFIG_TEST       0
#define HLH_DEBUG_CONFIG_MAIN       1


//========================  Debug Options Offsets  =============================

#define HLH_DEBUG_BIT_OFFSET_AUDIO  1
#define HLH_DEBUG_BIT_OFFSET_LUDP   2
#define HLH_DEBUG_BIT_OFFSET_JITTER 3
#define HLH_DEBUG_BIT_OFFSET_UTILS  4
#define HLH_DEBUG_BIT_OFFSET_VIDEO  5
#define HLH_DEBUG_BIT_OFFSET_VPHONE 6
#define HLH_DEBUG_BIT_OFFSET_FB     7
#define HLH_DEBUG_BIT_OFFSET_PP     8
#define HLH_DEBUG_BIT_OFFSET_MFC    9

#define HLH_DEBUG_BIT_OFFSET_TEST   30
#define HLH_DEBUG_BIT_OFFSET_MAIN   31


//========================  Debug Options Flags  ===============================

#define HLH_DEBUG_AUDIO      (  ( HLH_DEBUG_CONFIG_AUDIO  & 0x01 ) << HLH_DEBUG_BIT_OFFSET_AUDIO   )
#define HLH_DEBUG_LUDP       (  ( HLH_DEBUG_CONFIG_LUDP   & 0x01 ) << HLH_DEBUG_BIT_OFFSET_LUDP    )
#define HLH_DEBUG_JITTER     (  ( HLH_DEBUG_CONFIG_JITTER & 0x01 ) << HLH_DEBUG_BIT_OFFSET_JITTER  )
#define HLH_DEBUG_UTILS      (  ( HLH_DEBUG_CONFIG_UTILS  & 0x01 ) << HLH_DEBUG_BIT_OFFSET_UTILS   )
#define HLH_DEBUG_VIDEO      (  ( HLH_DEBUG_CONFIG_VIDEO  & 0x01 ) << HLH_DEBUG_BIT_OFFSET_VIDEO   )
#define HLH_DEBUG_VPHONE     (  ( HLH_DEBUG_CONFIG_VPHONE & 0x01 ) << HLH_DEBUG_BIT_OFFSET_VPHONE  )
#define HLH_DEBUG_FB         (  ( HLH_DEBUG_CONFIG_FB     & 0x01 ) << HLH_DEBUG_BIT_OFFSET_FB      )
#define HLH_DEBUG_PP         (  ( HLH_DEBUG_CONFIG_PP     & 0x01 ) << HLH_DEBUG_BIT_OFFSET_PP      )
#define HLH_DEBUG_MFC        (  ( HLH_DEBUG_CONFIG_MFC    & 0x01 ) << HLH_DEBUG_BIT_OFFSET_MFC     )

#define HLH_DEBUG_TEST       (  ( HLH_DEBUG_CONFIG_TEST   & 0x01 ) << HLH_DEBUG_BIT_OFFSET_TEST    )
#define HLH_DEBUG_MAIN       (  ( HLH_DEBUG_CONFIG_MAIN   & 0x01 ) << HLH_DEBUG_BIT_OFFSET_MAIN    )






/******************************************************************************
 * Desc : Combination of the Debug Configurations
 ******************************************************************************/
#define HLH_DEBUG_FLAGS       \
  ( HLH_DEBUG_AUDIO | HLH_DEBUG_LUDP  | HLH_DEBUG_JITTER | \
    HLH_DEBUG_UTILS | HLH_DEBUG_VIDEO | HLH_DEBUG_VPHONE | \
    HLH_DEBUG_FB    | HLH_DEBUG_PP    | HLH_DEBUG_MFC    | \
    HLH_DEBUG_TEST  | HLH_DEBUG_MAIN  )



//===========================  Use fake audio  =================================
#define __USE_FAKE_AUDIO__          0

#define __USE_FAKE_SWAP__           1
#if (__USE_FAKE_SWAP__ <= 0)
# define __USE_FAKE_AUDIO_IN__      0
# define __USE_FAKE_AUDIO_OUT__     1
#else
# define __USE_FAKE_AUDIO_IN__      1
# define __USE_FAKE_AUDIO_OUT__     0
#endif



#endif /* __CONFIG_INC_20091027_134502_HENRY__ */
